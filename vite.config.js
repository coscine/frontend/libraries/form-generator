import path from 'node:path';
import { defineConfig } from 'vite';
import vue from "@vitejs/plugin-vue";
import { nodePolyfills } from 'vite-plugin-node-polyfills';
import dts from 'vite-plugin-dts';
import Components from "unplugin-vue-components/vite";
import AutoImport from 'unplugin-auto-import/vite'
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import { BootstrapVueNextResolver } from "unplugin-vue-components/resolvers";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
     "@": `${path.resolve(__dirname, "src")}`,
    },
  },
  build: {
    minify: true,
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: '@coscine/form-generator',
      fileName: 'index',
      formats: ['es'],
    },
    rollupOptions: {
      external: [
        '@vueuse/core',
        '@zazuko/prefixes',
        'buffer',
        'rdf-parse',
        'rdf-validate-shacl',
        'vue',
      ],
      output: {
        // Provide global variables to use in the UMD build
        // Add external deps here
        globals: {
          '@zazuko/prefixes': 'ZazukoPrefixes',
          buffer: 'Buffer',
          'rdf-parse': 'RdfParser',
          'rdf-validate-shacl': 'SHACLValidator',
          vue: 'Vue',
        },
        inlineDynamicImports: false,
      },
    },
  },
  plugins: [
    Components({
      resolvers: [
        BootstrapVueNextResolver(),
        IconsResolver(),
      ],
      dts: "src/components.d.ts",
    }),
    Icons({
      compiler: 'vue3',
      autoInstall: true,
      scale: 1,
      defaultClass: "unplugin-icon unplugin-icon-form-generator"
    }),
    AutoImport({      
      imports: ["vue"],
      dts: "src/auto-imports.d.ts",
      eslintrc: {
        enabled: true, // <-- this
      },
      resolvers: [
        BootstrapVueNextResolver(),
        IconsResolver(),
      ],
      dirs: [
        './src'
      ],
    }),
    nodePolyfills(),
    vue(),
    dts(),
  ],  
  test: {
    globals: true,
    environment: 'happy-dom',
    reporters: 'dot',
  },
});
