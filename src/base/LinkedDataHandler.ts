import { defineComponent } from 'vue';

import factory from 'rdf-ext';
import SHACLValidator from 'rdf-validate-shacl';
import prefixes from '@zazuko/prefixes/prefixes';
import type { Dataset, Quad_Object, Quad_Subject } from '@rdfjs/types';
import type { ValidationType } from '@/types/validationTypes';
import QuadExt from 'rdf-ext/lib/Quad';
import type ValidationReport from 'rdf-validate-shacl/src/validation-report';
import type { FixedValues } from '@/types/fixedValues';
import DatasetExt from 'rdf-ext/lib/Dataset';

export default defineComponent({
  setup() {
    const emit = defineEmits<{
      (e: 'isValid', report: ValidationReport, context: string): void;
      (e: 'isValidating', isValidating: boolean, context: string): void;
      (e: 'update:fixed-values', dataset: Dataset): void;
      (
        e: 'update:form-data',
        fixedValues: FixedValues,
        object: Quad_Object,
      ): void;
    }>();
    return { emit };
  },
  data() {
    return {
      errorMessages: {} as { [nodeName: string]: Quad_Object[] },
      timeOuts: {} as Record<string, number>,
      validationDebounce: 500,
    };
  },
  methods: {
    fillResults(currentResults: ValidationType, results: ValidationType) {
      for (const currentResult of currentResults) {
        results.push(currentResult);
        if (currentResult.detail) {
          this.fillResults(currentResult.detail, results);
        }
      }
    },
    setErrorMessages(
      metadata: Dataset,
      results: ValidationType,
      subjectNode: Quad_Subject,
    ) {
      for (const result of results) {
        if (result.path) {
          const path = result.path;
          if (
            result.focusNode &&
            result.focusNode.value === subjectNode.value &&
            metadata.some(
              (entry) =>
                entry.predicate.value === path.value &&
                entry.object.value !== '',
            )
          ) {
            this.errorMessages[path.value] = result.message;
          }
        }
      }
    },
    triggerMetadataValidation(
      metadata: Dataset,
      dataset: Dataset,
      subjectNode: Quad_Subject,
      validationContext = '',
      ignoreTemplateValues = false,
    ): void {
      if (this.timeOuts[validationContext]) {
        clearTimeout(this.timeOuts[validationContext]);
      }
      // Do not try to validate empty metadata sets
      if (metadata.size === 0) {
        return;
      }
      const clonedMetadata = factory.dataset(
        Array.from(metadata),
      ) as unknown as Dataset;
      const clonedDataset = (
        dataset as unknown as DatasetExt
      ).clone() as unknown as Dataset;
      const clonedValidationContext = validationContext + '';
      this.$emit('isValidating', true, clonedValidationContext);
      this.timeOuts[validationContext] = setTimeout(async () => {
        const combinedMetadata = factory.dataset(Array.from(clonedMetadata));

        // rdfs:subClassOf definitions have to be included for the validation to work with inheritance
        const subClasses = clonedDataset.match(
          null,
          factory.namedNode(prefixes.rdfs + 'subClassOf'),
        );
        for (const subClassQuad of subClasses) {
          combinedMetadata.add(subClassQuad as QuadExt);
        }

        // Describe that the selected value for a class relationship is a value of that class to satisfy validation
        const pathRelations = clonedDataset.match(
          null,
          factory.namedNode(prefixes.sh + 'path'),
        );
        const classRelations = Array.from(pathRelations).map((pathRelation) => {
          return {
            path: pathRelation.object,
            classRelations: clonedDataset.match(
              pathRelation.subject,
              factory.namedNode(prefixes.sh + 'class'),
            ),
          };
        });
        for (const classRelation of classRelations.filter(
          (entry) => entry.classRelations && entry.classRelations.size,
        )) {
          const metadataRelations = clonedMetadata.match(
            undefined,
            classRelation.path,
          );
          if (metadataRelations.size) {
            for (const metadataRelation of metadataRelations) {
              for (const classEntry of classRelation.classRelations) {
                combinedMetadata.add(
                  factory.quad(
                    metadataRelation.object as Quad_Subject,
                    factory.namedNode(prefixes.rdf + 'type'),
                    classEntry.object,
                  ),
                );
              }
            }
          }
        }

        if (ignoreTemplateValues) {
          this.deactiveTemplateRules(clonedDataset, combinedMetadata);
        }

        const validator = new SHACLValidator(clonedDataset);
        const report = validator.validate(combinedMetadata);

        this.$emit('isValid', report, clonedValidationContext);

        this.errorMessages = {};
        const results: ValidationType = [];
        this.fillResults(report.results, results);
        this.setErrorMessages(clonedMetadata, results, subjectNode);

        this.$emit('isValidating', false, clonedValidationContext);
        return report;
      }, this.validationDebounce) as unknown as number;
    },
    deactiveTemplateRules(shape: Dataset, metadata: DatasetExt) {
      for (const quad of metadata) {
        if (!quad.object.value.match(/\{\{.*?\}\}/)) {
          continue;
        }
        const pathRelations = Array.from(
          shape.match(
            null,
            factory.namedNode(prefixes.sh + 'path'),
            quad.predicate,
          ),
        ).map((x) => x.subject);
        for (const pathRelation of pathRelations) {
          shape.add(
            factory.quad(
              pathRelation,
              factory.namedNode(prefixes.sh + 'deactivated'),
              factory.literal(
                'true',
                factory.namedNode(prefixes.xsd + 'boolean'),
              ),
            ),
          );
        }
      }
    },
  },
});
