export interface ValueType {
  datatype?: string;
  type: 'literal' | 'uri' | 'bnode';
  value: string;
  targetClass?: string;
}

export const DefaultFixedValue = 'https://purl.org/coscine/defaultValue';
export const FixedFixedValue = 'https://purl.org/coscine/fixedValue';
export const InvisibleFixedValue = 'https://purl.org/coscine/invisible';

export type FixedValueObject = {
  [key in
    | typeof DefaultFixedValue
    | typeof FixedFixedValue
    | typeof InvisibleFixedValue]?: ValueType[];
};

export interface FixedValues {
  [identifier: string]: FixedValueObject;
}
