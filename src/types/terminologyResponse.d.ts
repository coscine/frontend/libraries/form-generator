export interface TerminologyResponse {
  responseHeader: ResponseHeader;
  response?: Response;
  facet_counts: FacetCounts;
  highlighting: { [key: string]: FacetDates };
}

export interface FacetCounts {
  facet_queries: FacetDates;
  facet_fields: FacetFields;
  facet_dates: FacetDates;
  facet_ranges: FacetDates;
  facet_intervals: FacetDates;
  facet_heatmaps: FacetDates;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FacetDates {}

export interface FacetFields {
  ontology_name: Array<number | string>;
  ontology_prefix: Array<number | string>;
  type: Array<number | string>;
  subset: Array<number | string>;
  is_defining_ontology: Array<number | string>;
  is_obsolete: Array<number | string>;
}

export interface Response {
  numFound: number;
  start: number;
  docs: Doc[];
}

export interface Doc {
  id: string;
  iri: string;
  short_form: string;
  label: string;
  ontology_name: Ontology;
  ontology_prefix: Ontology;
  type: Type;
  is_defining_ontology: boolean;
  obo_id?: string;
  description?: string[];
}

export enum Ontology {
  DR = 'dr',
  Premis3 = 'premis3',
}

export enum Type {
  Class = 'class',
  Individual = 'individual',
}

export interface ResponseHeader {
  status: number;
  QTime: number;
  params: Params;
}

export interface Params {
  'facet.field': string[];
  hl: string;
  fl: string;
  start: string;
  fq: string[];
  rows: string;
  'hl.simple.pre': string;
  bq: string;
  q: string;
  defType: string;
  'hl.simple.post': string;
  qf: string;
  'hl.fl': string[];
  facet: string;
  wt: string;
}
