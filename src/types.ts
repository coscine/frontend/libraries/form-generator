import type { Dataset } from '@rdfjs/types';

export interface FormGeneratorState {
  internalDataset: Dataset;
}
